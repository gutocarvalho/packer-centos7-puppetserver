#!/bin/bash

wget http://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
yum install -y puppetlabs-release-pc1-el-7.noarch.rpm
rm -f puppetlabs-release-pc1-el-7.noarch.rpm
yum install -y puppet-agent puppetserver
echo 127.0.0.1 puppet >> /etc/hosts
systemctl start puppetserver
systemctl enable puppetserver
