==========================
gutocarvalho-packer-centos7-puppetserver
==========================

Packer templates for building base VM boxes for Virtualbox.

Usage
=====

Installing Packer
-----------------

If you're using Homebrew

::

    $ brew tap homebrew/binary
    $ brew install packer


Running Packer
--------------

::

    $ git clone https://github.com/gutocarvalho/packer-centos7-puppetserver.git
    $ cd packer-centos7-puppetserver
    $ packer build packer.json

Supported versions
------------------

This templates was tested using a packer 0.9.0, vagrant 1.8.5 and vbox 5.0.26
